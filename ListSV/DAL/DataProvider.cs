﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListSV.DAL
{
    public class DataProvider
    {
        private static DataProvider _Instance;
        private string strcnn = @"Data Source=DESKTOP-5E2U87G;Initial Catalog=SvDB;Integrated Security=True;Connect Timeout=30";

        public static DataProvider Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new DataProvider();
                return _Instance;
            }
            private set => _Instance = value;
        }
        private DataProvider()
        {

        }
        public DataTable GetRecord(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection cnn = new SqlConnection(strcnn))
                {
                    SqlDataAdapter da = new SqlDataAdapter(query, cnn);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool ExecuteDB(string query, params object[] data) 
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(strcnn))
                {
                    SqlCommand cmd = new SqlCommand(query, cnn);
                    if (query.IndexOf("insert") != -1 || query.IndexOf("update") != -1)
                    {
                        SqlParameter MSSV = new SqlParameter("@MSSV", typeof(string));
                        SqlParameter NameSV = new SqlParameter("@NameSV", typeof(string));
                        SqlParameter Gender = new SqlParameter("@Gender", typeof(int));
                        SqlParameter Birthday = new SqlParameter("@Birthday", typeof(DateTime));
                        SqlParameter DiaChi = new SqlParameter("@DiaChi", typeof(string));
                        SqlParameter Phone = new SqlParameter("@Phone", typeof(string));
                        SqlParameter LopHP = new SqlParameter("@LopHP", typeof(string));
                       
                        MSSV.Value = data[0];
                        NameSV.Value = data[1];
                        Gender.Value = Convert.ToInt32(data[2]);
                        Birthday.Value = data[3];
                        DiaChi.Value = data[4];
                        Phone.Value = data[5];
                        LopHP.Value = data[6];
                        cmd.Parameters.AddRange(new SqlParameter[] 
                        { 
                            MSSV, 
                            NameSV,
                            Gender,
                            Birthday,
                            DiaChi,
                            Phone,
                            LopHP,
                         });
                    }
                    if(query.IndexOf("delete") != -1)
                    {
                        SqlParameter MSSV = new SqlParameter("@MSSV", typeof(string));

                        MSSV.Value = data[0];
                        cmd.Parameters.Add(MSSV);
                    }
                    cnn.Open();
                    int n = (int)cmd.ExecuteNonQuery();
                    cnn.Close();
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }
    }
}
