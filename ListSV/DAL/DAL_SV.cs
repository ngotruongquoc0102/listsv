﻿using ListSV.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListSV.DAL
{
    public class DAL_SV
    {
        private static DAL_SV _Instance;

        private DAL_SV()
        {

        }

        public static DAL_SV Instance
        {
            get
            {
                if (_Instance == null)
                    _Instance = new DAL_SV();
                return _Instance;
            }
            private set => _Instance = value;
        }
        public List<SinhVien> GetListSV_DAL()
        {
            List<SinhVien> data = new List<SinhVien>();
            string query = "select * from SinhVien";
            try
            {
                foreach (DataRow item in DataProvider.Instance.GetRecord(query).Rows)
                {
                    data.Add(GetSV(item));
                }
            }
            catch (Exception)
            {
                return null;
            }
            return data;
        }
        public SinhVien GetSV(DataRow r)
        {
            return new SinhVien
            {
                MSSV = r["MSSV"].ToString().Trim(),
                Name = r["NameSV"].ToString().Trim(),
                Gender = Convert.ToBoolean(r["Gender"]),
                Birthday = Convert.ToDateTime(r["Birthday"].ToString()),
                DiaChi = r["DiaChi"].ToString().Trim(),
                Phone = r["Phone"].ToString().Trim(),
                LopHP = r["LopHP"].ToString().Trim(),
            };
        }
        public bool AddSV_DAL(params object[] data)
        {
            try
            {
                string query = "insert into SinhVien values(@MSSV, @NameSV, @Gender, @Birthday, @DiaChi, @Phone, @LopHP)";
                
                return DataProvider.Instance.ExecuteDB(query, data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        public bool DestroySV_DAL(List<string> sv)
        {
            try
            {
                bool state;
                foreach (String item in sv)
                {
                    string query = "delete from SinhVien where MSSV=@MSSV";
                    state = DataProvider.Instance.ExecuteDB(query, item);
                    if(state == false)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
           
        }
        public bool UpdateSV_DAL(params object[] data)
        {
            try
            {
                string query = "update SinhVien set NameSV=@NameSV, Gender=@Gender, Birthday=@Birthday, DiaChi=@DiaChi, Phone=@Phone, LopHP=@LopHP where MSSV=@MSSV";
                return DataProvider.Instance.ExecuteDB(query, data);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }
    }
}
