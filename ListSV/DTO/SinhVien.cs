﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListSV.DTO
{

    public class SinhVien
    {
        private string _MSSSV;
        private string _Name;
        private bool _Gender;
        private DateTime _Birthday;
        private string _DiaChi;
        private string _Phone;
        private string _LopHP;
        
        public string MSSV { get => _MSSSV; set => _MSSSV = value; }
        public string Name { get => _Name; set => _Name = value; }
        public bool Gender { get => _Gender; set => _Gender = value; }
        public DateTime Birthday { get => _Birthday; set => _Birthday = value; }
        public string DiaChi { get => _DiaChi; set => _DiaChi = value; }
        public string Phone { get => _Phone; set => _Phone = value; }
        public string LopHP { get => _LopHP; set => _LopHP = value; }
    }
}
