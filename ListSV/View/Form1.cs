﻿using ListSV.BLL;
using ListSV.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListSV
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CreateView();
            sortProperty.SelectedIndex = 0;
            GetCBB(lopHp);
            lopHp.Items.Add("All");

        }
        //Get data from database into List<SinhVien>
        public void CreateView()
        {
            BLL_SV.Instance.GetListSV_BLL();
        }
        private void search_Click(object sender, EventArgs e)
        {
            try
            {
                if (lopHp.Text != "")
                {
                    BLL_SV.Instance.GetListSV_BLL();
                    DataSV.DataSource = GetSvByLopHP(lopHp.SelectedItem.ToString());
                }
                else
                {
                    MessageBox.Show("Vui long Chon lop hp");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
        public List<SinhVien> GetSvByLopHP(string txtLopHp)
        {
            List<SinhVien> data = new List<SinhVien>();
            if (txtLopHp == "All")
            {
                if (search_input.Text != "")
                {
                    try
                    {
                        foreach (SinhVien s in BLL_SV.Instance.SV)
                        {
                            if (string.Compare(s.Name, search_input.Text) == 0)
                            {
                                data.Add(new SinhVien { MSSV = s.MSSV, Name = s.Name, Gender = s.Gender, Birthday = s.Birthday, DiaChi = s.DiaChi, Phone = s.Phone, LopHP = s.LopHP });
                            }
                        }
                        return data;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    data = BLL_SV.Instance.SV;
                }
            }
            else
            {
                if (search_input.Text == "")
                {
                    foreach (SinhVien s in BLL_SV.Instance.SV)
                    {
                        if (s.LopHP == txtLopHp)
                        {
                            data.Add(new SinhVien { MSSV = s.MSSV, Name = s.Name, Gender = s.Gender, Birthday = s.Birthday, DiaChi = s.DiaChi, Phone = s.Phone, LopHP = s.LopHP });
                        }
                    }
                }
                else
                {
                    foreach (SinhVien s in BLL_SV.Instance.SV)
                    {
                        if (s.LopHP == txtLopHp && string.Compare(s.Name, search_input.Text) == 0)
                        {
                            data.Add(new SinhVien { MSSV = s.MSSV, Name = s.Name, Gender = s.Gender, Birthday = s.Birthday, DiaChi = s.DiaChi, Phone = s.Phone, LopHP = s.LopHP });
                        }
                    }
                }
            }
            return data;
        }

        //Add Student

        private void add_Click(object sender, EventArgs e)
        {
            string MSSVTxt = mssv.Text;
            string NameTxt = name.Text;
            string LopHPTxt = Convert.ToString(comboBoxLopHp.SelectedItem);
            string phoneTxt = Phone.Text;
            DateTime birthdayTxt = Convert.ToDateTime(dateTimePicker1.Value);
            string DiaChiTxt = diachi.Text;
            bool MaleChecked = Male.Checked;
            bool FemaleChecked = Female.Checked;
            if (BLL_SV.Instance.CheckFieldData(new object[] {
              MSSVTxt, NameTxt, LopHPTxt, phoneTxt, birthdayTxt, DiaChiTxt, MaleChecked, FemaleChecked
            }))
            {
                if (BLL_SV.Instance.Add(MSSVTxt, NameTxt, MaleChecked, birthdayTxt, DiaChiTxt, phoneTxt, LopHPTxt))
                {
                    lopHp.SelectedIndex = lopHp.Items.Count - 1;
                    BLL_SV.Instance.GetListSV_BLL();
                    DataSV.DataSource = GetSvByLopHP("All");
                }
            }
            else
            {
                MessageBox.Show("Make sure you fill up all the field");
            }
        }
        private void DataSV_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewSelectedRowCollection data = DataSV.SelectedRows;
            try
            {
                if (data.Count == 1)
                {
                    string MSSVtxt = data[0].Cells["MSSV"].Value.ToString();
                    SinhVien GetSV = BLL_SV.Instance.GetSVByMSSV(MSSVtxt);
                    if (GetSV != null)
                    {
                        mssv.Text = GetSV.MSSV;
                        mssv.ReadOnly = true;
                        name.Text = GetSV.Name;
                        Phone.Text = GetSV.Phone;
                        dateTimePicker1.Value = Convert.ToDateTime(GetSV.Birthday);
                        diachi.Text = GetSV.DiaChi;
                        if (Convert.ToBoolean(GetSV.Gender))
                            Male.Checked = true;
                        else
                            Female.Checked = true;
                        GetCBB(comboBoxLopHp);
                        int index = comboBoxLopHp.Items.IndexOf(GetSV.LopHP);
                        comboBoxLopHp.SelectedIndex = index;
                    }
                    else
                    {
                        MessageBox.Show("Invalid selected Item");
                    }

                }
                else
                {
                    Console.WriteLine("Invalid selected rows in table");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                throw;
            }
        }
        private void update_Click(object sender, EventArgs e)
        {
            string MSSVTxt = mssv.Text.Trim();
            string NameTxt = name.Text.Trim();
            string LopHPTxt = Convert.ToString(comboBoxLopHp.SelectedItem);
            string phoneTxt = Phone.Text;
            DateTime birthdayTxt = Convert.ToDateTime(dateTimePicker1.Value);
            string DiaChiTxt = diachi.Text;
            bool MaleChecked = Male.Checked;
            bool FemaleChecked = Female.Checked;
            if (BLL_SV.Instance.CheckFieldData(new object[] {
              MSSVTxt, NameTxt, LopHPTxt, phoneTxt, birthdayTxt, DiaChiTxt, MaleChecked, FemaleChecked
            }))
            {
                if (BLL_SV.Instance.UpdateSV(MSSVTxt, NameTxt, MaleChecked, birthdayTxt, DiaChiTxt, phoneTxt, LopHPTxt))
                {
                    lopHp.SelectedItem = LopHPTxt;
                    DataSV.DataSource = GetSvByLopHP(LopHPTxt);
                }
            }
            else
                MessageBox.Show("Make sure you fill up all fields");
        }
        public void GetCBB(ComboBox cb)
        {
            if (cb != null)
            {
                cb.Items.Clear();
            }
            Console.WriteLine(cb);
            cb.Items.AddRange(GetLopHP().Distinct().ToArray());
        }
        public List<string> GetLopHP()
        {
            List<string> data = new List<string>();
            foreach (SinhVien s in BLL_SV.Instance.SV)
            {
                data.Add(s.LopHP);
            }
            return data;
        }
        private void sort_Click(object sender, EventArgs e)
        {
            BLL_SV.Instance.SV = BLL_SV.Instance.Sort(BLL_SV.Instance.SV.ToArray(), BLL_SV.CompareName);
            string lop = lopHp.SelectedItem.ToString();
            DataSV.DataSource = GetSvByLopHP(lop);
        }
        public bool DeleteSV(List<string> data)
        {
            return BLL_SV.Instance.DestroySV(data);
        }
        private void del_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection data = DataSV.SelectedRows;
            List<string> GetSV = new List<string>();
            try
            {
                if (data.Count != 0)
                {
                    foreach (DataGridViewRow r in data)
                    {
                        GetSV.Add(r.Cells["MSSV"].Value.ToString().Trim());
                    }
                    if (DeleteSV(GetSV))
                    {
                        string lop = lopHp.SelectedItem.ToString();
                        BLL_SV.Instance.GetListSV_BLL();
                        DataSV.DataSource = GetSvByLopHP(lop);
                    }
                    else
                    {
                        MessageBox.Show("Error");
                    }
                }
                else
                {
                    MessageBox.Show("Error");
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

    }
}
