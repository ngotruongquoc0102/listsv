﻿using ListSV.DAL;
using ListSV.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListSV.BLL
{
    public class BLL_SV
    {
        private List<SinhVien> _SV;
        private static BLL_SV _Instance;
        public static BLL_SV Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new BLL_SV();
                }
                return _Instance;
            }
            private set => _Instance = value;
        }
        private BLL_SV()
        {

        }
        public List<SinhVien> SV { get => _SV; set => _SV = value; }

        public void GetListSV_BLL()
        {
            try
            {
                SV = DAL_SV.Instance.GetListSV_DAL();
            }
            catch (Exception error)
            {
                throw error;
            }
           
        }
        public bool Add(params object[] data)
        {
            bool state = DAL_SV.Instance.AddSV_DAL(data);
            if(state)
            {
                return true;
            }
            return false;
        }
        public SinhVien GetSVByMSSV(string mssv)
        {
            SinhVien data = new SinhVien();
            try
            {
                foreach (SinhVien s in BLL_SV.Instance.SV)
                {
                    if (s.MSSV == mssv)
                    {
                        data = s;
                        return data;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }
        }
        public static bool CompareName(object obj1, object obj2)
        {
            if (string.Compare(((SinhVien)obj1).Name, ((SinhVien)obj2).Name) > 0)
            {
                return true;
            }
            return false;
        }
        public bool UpdateSV(params object[] data)
        {
            try
            {
                DAL_SV.Instance.UpdateSV_DAL(data);
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CheckFieldData(object[] data)
        {
            if (data[0].ToString() == "" || data[1].ToString() == "" || data[2].ToString() == "" || data[3].ToString() == ""
                || data[4].ToString() == "" || data[5].ToString() == "" || (data[6].ToString() == "False" && data[7].ToString() == "False")
                )
            {
                return false;
            }

            return true;
        }

        public delegate bool MyCompare(object obj1, object obj2);

        public List<SinhVien> Sort(Object[] arr, MyCompare cmp)
        {
            List<SinhVien> data = new List<SinhVien>();
            for (int i = 0; i < arr.Length - 1; i++)
            {
                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (cmp(arr[i], arr[j]))
                    {
                        object newSV = arr[i];
                        arr[i] = arr[j];
                        arr[j] = newSV;
                    }
                }
            }
            foreach (SinhVien s in arr)
            {
                data.Add(s);
            }
            return data;
        }
        public bool DestroySV(List<string> sv)
        {
            bool sate = DAL_SV.Instance.DestroySV_DAL(sv);
            if (sate) return true;
            return false;
        }
    }
}
